class Fixnum
  def in_words
    if self < 10
      self.one_dig
    elsif self >= 10 && self < 20
      self.teen_nums
    elsif self >= 20 && self < 100
      self.under_100
    else
      self.larger_nums
    end
  end

  # helper methods
  def one_dig
    ones = {
      1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five",
      6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 0 => "zero",
    }
    ones[self]
  end

  def teen_nums
    teens = {
      10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen",
      14 => "fourteen", 15 => "fifteen", 16 => "sixteen",
      17 => "seventeen", 18 => "eighteen", 19 => "nineteen"
    }
    teens[self]
  end

  def under_100
    tens = {
      2 => "twenty", 3 => "thirty", 4 => "forty", 5 => "fifty",
      6 => "sixty", 7 => "seventy", 8 => "eighty", 9 => "ninety"
    }
    num_str = self.to_s
    word = tens[num_str[0].to_i]
    unless num_str[1] == "0"
      word += " #{num_str[1].to_i.one_dig}"
    end
    word
  end

  def larger_nums
    number_data = {
      100 => "hundred", 1_000 => "thousand", 1_000_000 => "million",
      1_000_000_000 => "billion", 1_000_000_000_000 => "trillion"
    }
    scale = self.len
    letter_num = number_data[scale]
    remain = self % scale
    place = (self - remain) / scale
    word = "#{place.in_words} #{letter_num}"
    unless remain == 0
      word += " #{remain.in_words}"
    end
    word
  end

  def len
    leng = self.to_s.length
    if leng == 3
      scale = 100
    elsif leng > 3 && leng < 7
      scale = 1000
    elsif leng >= 7 && leng < 10
      scale = 1_000_000
    elsif leng >= 10 && leng < 13
      scale = 1_000_000_000
    else
      scale = 1_000_000_000_000
    end
    scale
  end
end
